from subprocess import Popen, PIPE
import sys
import traceback
import re
import time

from keyboard_backlight_control import *

# NOTE: on my oryx the LEDs seem to be going bad, so brightest uniform white seems to be:
# 2020ff 2020ff 203060

sleep_time = 2.0

def lerp(a, b, value):
    return (b - a) * value + a

def color_lerp(a, b, value):
    return Color((lerp(a.r, b.r, value), lerp(a.g, b.g, value), lerp(a.b, b.b, value)))

def bar_colors(a, b, value, res):
    l = []
    for i in range(res):
        v = value * res - i
        if v <= 0:
            l.append(a)
        elif v >= 1:
            l.append(b)
        else:
            l.append(color_lerp(a, b, v))
    return l

def grad(a, b, res):
    if res == 1:
        return [a]
    l = []
    for i in range(res):
        l.append(color_lerp(a, b, float(i) / (res - 1.0)))
    return l

def run_acpi_cmd():
    #return 'Battery 0: Discharging, 20%, 02:42:35 remaining'
    p = Popen(['acpi', '-b'], stdout=PIPE)
    stdout, _ = p.communicate()
    return stdout.decode('utf-8') if stdout != None else ''

class ChargingState:
    def __init__(self, percent_str):
        self.on = Color('8000FF')
        self.off = Color((0, 0, 0))
        self.set_v(percent_str)

    def set_v(self, percent_str):
        self.v = float(percent_str) / 100.0

    def is_valid(self):
        return self.v > 0

    def show(self, color_ctrl):
        color_ctrl.set_list(bar_colors(self.off, self.on, self.v, color_ctrl.get_num()))

class DischargingState:
    def __init__(self, percent_str):
        self.offset = 0.025 # make it look like it hits zero when it actually hits this
        self.high = Color('00FFFF')
        self.low = Color('00FF00')
        self.very_low = Color('FF8000')
        self.low_thresh = 0.2
        self.off = Color((0, 0, 0))
        self.set_v(percent_str)

    def set_v(self, percent_str):
        self.v = float(percent_str) / 100.0
        self.v = self.v * (1 + self.offset) - self.offset

    def is_valid(self):
        return self.v > 0

    def show(self, color_ctrl):
        if self.v <= self.low_thresh:
            color_ctrl.set_list(bar_colors(self.off, self.very_low, self.v / self.low_thresh, color_ctrl.get_num()))
        else:
            color_ctrl.set_list(bar_colors(self.off, color_lerp(self.low, self.high, self.v), self.v, color_ctrl.get_num()))

class FullState:

    def show(self, color_ctrl):

        if color_ctrl.get_num() == 3:
            # This color is nice and transy on my Oryx because the blue is dim on the left two lights
            # NOTE: 9010FF 4050FF 0040B0 used to work, but became bad
            color_ctrl.set_list([Color(c) for c in '9015FF 2030FF 0020C0'.split()])
        else:
            color_ctrl.set_list(grad(Color('00FF00'), Color('0000FF'), color_ctrl.get_num()))

    def is_valid(self):
        return True

class ErrorState:

    def show(self, color_ctrl):
        color_ctrl.set_single(Color('FF8000'))

def parse_acpi_line(acpi):
    discharging = re.findall('Discharging, (\d+)%', acpi)
    charging = re.findall('(Charging|Unknown), (\d+)%', acpi)
    full = re.findall('Full', acpi)
    if len(discharging) == 1 and len(charging) == 0 and len(full) == 0:
        return DischargingState(discharging[0])
    elif len(discharging) == 0 and len(charging) == 1 and len(full) == 0:
        return ChargingState(charging[0][1])
    elif len(discharging) == 0 and len(charging) == 0 and len(full) == 1:
        return FullState()
    else:
        raise RuntimeError('acpi could not be parsed:\n' +
              '> ' + acpi + '\n' +
              'charging: ' + str(charging) + '\n' +
              'discharging: ' + str(discharging) + '\n' +
              'full: ' + str(full))

def get_power_state():
    acpi = run_acpi_cmd()
    states = []
    errors = []
    for line in acpi.splitlines():
        try:
            states.append(parse_acpi_line(line))
        except RuntimeError as e:
            errors.append(str(e))
    for state in states:
        if state.is_valid():
            return state
        else:
            errors.append('Invalid state')
    if not errors:
        errors.append('No batteries detected')
    print('\n'.join(errors), file=sys.stderr)
    return ErrorState()

def update(color_ctrl):
    try:
        state = get_power_state()
    except Exception:
        traceback.print_exc(file=sys.stderr)
        state = ErrorState()
    state.show(color_ctrl)

def run():
    factory = Factory()
    while True:
        update(factory.color_ctrl())
        time.sleep(sleep_time)

if __name__ == '__main__':
    try:
        run()
    except KbError as e:
        print(str(e), file=sys.stderr)
        exit(1)
    except Exception as e:
        traceback.print_exc(file=sys.stderr)
        exit(1)
